#include "l4_io/mp3_module.h"

#include "common_macros.h"
#include "delay.h"
#include "l3_drivers/gpio.h"
#include "task.h"

#define GENUINE 0

typedef enum {
  mp3_module__play_next = 0x01,
  mp3_module__play_previous = 0x02,
  mp3_module__play_index = 0x03, // accept value from 0 to 2999
  mp3_module__increase_volume = 0x04,
  mp3_module__decrease_volume = 0x05,
  mp3_module__set_volume = 0x06, // accept value from 0 to 30
  mp3_module__select_playback_device = 0x09,
  mp3_module__reset = 0x0C,
  mp3_module__resume_playing = 0x0D,
  mp3_module__pause_playing = 0x0E
} mp3_module__commands_e;

typedef struct {
  gpio__port_e gpio_port_for_uart_TX;
  uint8_t gpio_pin;
  gpio__function_e gpio_function;
} uart_gpio_s;

const static uart_gpio_s uart_gpio[4] = {{.gpio_port_for_uart_TX = 0, .gpio_pin = 0, .gpio_function = 0b100},
                                         {.gpio_port_for_uart_TX = 2, .gpio_pin = 0, .gpio_function = 0b010},
                                         {.gpio_port_for_uart_TX = 0, .gpio_pin = 10, .gpio_function = 0b001},
                                         {.gpio_port_for_uart_TX = 4, .gpio_pin = 28, .gpio_function = 0b010}};

const static uint8_t start = 0x7E;
const static uint8_t version = 0xFF;
const static uint8_t length = 0x06;
const static uint8_t feedback = 0x00;
const static uint8_t end = 0xEF;

static bool mp3_module__uart_init(uart_e uart);
static void mp3_module__gpio_set_to_uart(uart_e uart);
static int16_t mp3_module__checksum(uint8_t command, uint8_t parameter_1, uint8_t parameter_2);
static void mp3_module__uart_send_instruction(uart_e uart, uint8_t *instruction);
static void mp3_module__select_SD_card_as_device(uart_e uart);
static void mp3_module__set_volume_default(uart_e uart);
static void mp3_module__send_instruction(uart_e uart, uint8_t command, uint8_t parameter_1, uint8_t parameter_2);

void mp3_module__init(uart_e uart) {
  mp3_module__gpio_set_to_uart(uart);
  mp3_module__uart_init(uart);
  mp3_module__select_SD_card_as_device(uart);
  delay__ms(500);

#if GENUINE

  mp3_module__set_volume_default(uart);
  vTaskDelay(500);

#else

  UNUSED(mp3_module__set_volume_default);

#endif
}

void mp3_module__play(uart_e uart, uint32_t song_number) {
  if (song_number <= 2999) {
    const uint8_t no_parameter_1 = 0x00;
    mp3_module__send_instruction(uart, mp3_module__play_index, no_parameter_1, song_number);
  }
}

void mp3_module__resume(uart_e uart) {
  const uint8_t no_parameter = 0x00;
  mp3_module__send_instruction(uart, mp3_module__resume_playing, no_parameter, no_parameter);
}

void mp3_module__pause(uart_e uart) {
  const uint8_t no_parameter = 0x00;
  mp3_module__send_instruction(uart, mp3_module__pause_playing, no_parameter, no_parameter);
}

void mp3_module__reset_playback(uart_e uart) {
  const uint8_t no_parameter = 0x00;
  mp3_module__send_instruction(uart, mp3_module__reset, no_parameter, no_parameter);
}

static bool mp3_module__uart_init(uart_e uart) {
  bool status = false;
  const uint32_t UART_buad_rate = 9600;
  uart__init(uart, clock__get_peripheral_clock_hz(), UART_buad_rate);

  if (!uart__is_initialized(uart)) {
  } else {
    status = true;
  }

  return status;
}

static void mp3_module__gpio_set_to_uart(uart_e uart) {
  const uart_gpio_s gpio_info = uart_gpio[uart];

  gpio__construct_with_function(gpio_info.gpio_port_for_uart_TX, gpio_info.gpio_pin, gpio_info.gpio_function);
}

static int16_t mp3_module__checksum(uint8_t command, uint8_t parameter_1, uint8_t parameter_2) {
  const int16_t checksum = 0xFF - (version + length + command + feedback + parameter_1 + parameter_2) + 1;
  return checksum;
}

static void mp3_module__uart_send_instruction(uart_e uart, uint8_t *instruction) {
  uint8_t i = 0;
  while (i < 8) {
    if (uart__polled_put(uart, instruction[i])) {
      i++;
    } else {
      i = 100;
    }
  }
}

static void mp3_module__select_SD_card_as_device(uart_e uart) {
  const uint8_t playback_device_SD = 0x02;
  const uint8_t no_parameter_1 = 0x00;
  mp3_module__send_instruction(uart, mp3_module__select_playback_device, no_parameter_1, playback_device_SD);
}

static void mp3_module__set_volume_default(uart_e uart) {
  const uint8_t default_volume = 0x0A;
  const uint8_t no_parameter_2 = 0x00;
  mp3_module__send_instruction(uart, mp3_module__set_volume, default_volume, no_parameter_2);
}

// The module we use don't support checksum
// Lesson learned: Always read review if not buying a genuine product
// because some clone doesn't implement all of the original functions
// or better, just buy the genuine product because it saves you time
// If you are using a genuine df-mini player, change GENUINE to 1
static void mp3_module__send_instruction(uart_e uart, uint8_t command, uint8_t parameter_1, uint8_t parameter_2) {
#if GENUINE

  const int16_t checksum = mp3_module__checksum(command, parameter_1, parameter_2);
  const uint8_t checksum_lower_8_bits = checksum;
  const uint8_t checksum_upper_8_bits = checksum >> 8;

  uint8_t mp3_instruction[10] = {
      start, version, length, command, feedback, parameter_1, parameter_2, checksum_upper_8_bits, checksum_lower_8_bits,
      end};

#else

  uint8_t mp3_instruction[8] = {start, version, length, command, feedback, parameter_1, parameter_2, end};
  UNUSED(mp3_module__checksum);

#endif

  mp3_module__uart_send_instruction(uart, mp3_instruction);
}
