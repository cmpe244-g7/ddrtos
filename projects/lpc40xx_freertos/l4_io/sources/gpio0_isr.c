#include "gpio0_isr.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

static function_pointer_t gpio0_callbacks_falling[32];
static function_pointer_t gpio0_callbacks_rising[32];

static void gpio0_clear_pin_interrupt(uint32_t pin);
static int32_t gpio0_find_set_bit_falling(void);
static int32_t gpio0_find_set_bit_rising(void);

void gpio0__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio0__interrupt_dispatcher, "GPIO Interrupt");
  NVIC_EnableIRQ(GPIO_IRQn);

  if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
    gpio0_callbacks_falling[pin] = callback;
    LPC_GPIOINT->IO0IntEnF |= (1 << pin);
  } else {
    gpio0_callbacks_rising[pin] = callback;
    LPC_GPIOINT->IO0IntEnR |= (1 << pin);
  }
}

void gpio0__interrupt_dispatcher(void) {
  int32_t pin_that_generated_interrupt = gpio0_find_set_bit_falling();
  function_pointer_t attached_user_handler = NULL;

  if (pin_that_generated_interrupt == -1) {
    pin_that_generated_interrupt = gpio0_find_set_bit_rising();
    attached_user_handler = gpio0_callbacks_rising[pin_that_generated_interrupt];
  } else {
    attached_user_handler = gpio0_callbacks_falling[pin_that_generated_interrupt];
  }

  attached_user_handler();
  gpio0_clear_pin_interrupt(pin_that_generated_interrupt);
}

static void gpio0_clear_pin_interrupt(uint32_t pin) { LPC_GPIOINT->IO0IntClr |= (1 << pin); }

static int32_t gpio0_find_set_bit_falling(void) {
  uint32_t gpio0_interrupt_status_register_falling = LPC_GPIOINT->IO0IntStatF;

  uint32_t pin_number = 0;
  int32_t return_value = -1;

  while ((!(gpio0_interrupt_status_register_falling & (1 << pin_number))) && pin_number < 32) {
    pin_number++;
  }

  if (pin_number < 32) {
    return_value = pin_number;
  }

  return return_value;
}

static int32_t gpio0_find_set_bit_rising(void) {
  uint32_t gpio0_interrupt_status_register_rising = LPC_GPIOINT->IO0IntStatR;

  uint32_t pin_number = 0;
  int32_t return_value = -1;

  while ((!(gpio0_interrupt_status_register_rising & (1 << pin_number)) && pin_number < 32)) {
    pin_number++;
  }

  if (pin_number < 32) {
    return_value = pin_number;
  }

  return return_value;
}
