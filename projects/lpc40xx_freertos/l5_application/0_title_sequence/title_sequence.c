#include "title_sequence.h"

#include "button.h"
#include "gpio0_isr.h"
#include "l3_drivers/gpio.h"
#include "led_matrix.h"
#include "mp3_module.h"
#include "song_info.h"
#include "song_select.h"
#include "symbols.h"
#include "uart.h"

#include <stdbool.h>
#include <stdint.h>

static volatile bool press_to_start = false;

/***** HELPER FUNCTIONS *****/
static void draw_michael_hatzi_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time,
                                        color_code color);
static void draw_kyle_kwong_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time, color_code color);
static void draw_anthony_zunino_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time,
                                         color_code color);
static void draw_present_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time, color_code color);
static void draw_DDR_at_top_of_screen_and_RTOS_at_bottom(color_code DDR_color, color_code RTOS_color);
static void move_DDR_down_and_RTOS_up_line_by_line(uint32_t delay_between_shifts__ms, color_code DDR_color,
                                                   color_code RTOS_color);
static void merge_DDR_and_RTOS_and_make_R_new_color(color_code R_color, color_code DDR_color, color_code RTOS_color);
static void wait_then_draw_press_any_to_start(uint32_t wait_time__ms);
static void wait_for_button_input(void);
static void clear_board_and_wait(uint32_t wait_time__ms);
static void start_song_select_and_suspend_this_task(void);
static void wipe_screen_right_to_left(void);

void title_logo(void *p) {
  while (1) {
    set__game_state(title__state);
    mp3_module__play(song__mp3, ghost_voices);
    vTaskDelay(2400);

    const uint16_t wait_time__ms = 1000;
    draw_michael_hatzi_and_wait(10, 2, wait_time__ms, White);
    draw_kyle_kwong_and_wait(20, 10, wait_time__ms, White);
    draw_anthony_zunino_and_wait(30, 0, wait_time__ms, White);
    draw_present_and_wait(45, 14, wait_time__ms, White);

    vTaskDelay(wait_time__ms);
    draw_michael_hatzi_and_wait(10, 2, wait_time__ms, Black);
    vTaskDelay(wait_time__ms);
    draw_kyle_kwong_and_wait(20, 10, wait_time__ms, Black);
    vTaskDelay(wait_time__ms);
    draw_anthony_zunino_and_wait(30, 0, wait_time__ms, Black);
    vTaskDelay(wait_time__ms);
    draw_present_and_wait(45, 14, wait_time__ms, Black);

    vTaskDelay(1100);
    led_driver__clear_board();

    const color_code DDR = Blue;
    const color_code RTOS = Red;
    const color_code R = Purple;
    const uint32_t shift_delay__ms = 145;
    draw_DDR_at_top_of_screen_and_RTOS_at_bottom(DDR, RTOS);
    move_DDR_down_and_RTOS_up_line_by_line(shift_delay__ms, DDR, RTOS);
    merge_DDR_and_RTOS_and_make_R_new_color(R, DDR, RTOS);

    wait_then_draw_press_any_to_start(wait_time__ms);

    press_to_start = false;
    wait_for_button_input();
    mp3_module__play(sound_effect__mp3, 5);
    mp3_module__pause(song__mp3);

    clear_board_and_wait(wait_time__ms);

    start_song_select_and_suspend_this_task();
  }
}

void press_to_start__handler(void) { press_to_start = true; }

/***** HELPER FUNCTIONS BEGIN *****/
static void draw_michael_hatzi_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time,
                                        color_code color) {
  draw__M_small(row_position, col_position + 0, color);
  draw__I_small(row_position, col_position + 6, color);
  draw__C_small(row_position, col_position + 10, color);
  draw__H_small(row_position, col_position + 15, color);
  draw__A_small(row_position, col_position + 20, color);
  draw__E_small(row_position, col_position + 25, color);
  draw__L_small(row_position, col_position + 30, color);

  if (color != Black) {
    vTaskDelay(wait_time);
  }

  draw__H_small(row_position, col_position + 36, color);
  draw__A_small(row_position, col_position + 41, color);
  draw__T_small(row_position, col_position + 46, color);
  draw__Z_small(row_position, col_position + 52, color);
  draw__I_small(row_position, col_position + 57, color);
  if (color != Black) {
    vTaskDelay(wait_time);
  }
}

static void draw_kyle_kwong_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time, color_code color) {
  draw__K_small(row_position, col_position + 0, color);
  draw__Y_small(row_position, col_position + 5, color);
  draw__L_small(row_position, col_position + 10, color);
  draw__E_small(row_position, col_position + 15, color);
  if (color != Black) {
    vTaskDelay(wait_time);
  }

  draw__K_small(row_position, col_position + 21, color);
  draw__W_small(row_position, col_position + 26, color);
  draw__O_small(row_position, col_position + 32, color);
  draw__N_small(row_position, col_position + 37, color);
  draw__G_small(row_position, col_position + 42, color);
  if (color != Black) {
    vTaskDelay(wait_time);
  }
}

static void draw_anthony_zunino_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time,
                                         color_code color) {
  draw__A_small(row_position, col_position + 0, color);
  draw__N_small(row_position, col_position + 5, color);
  draw__T_small(row_position, col_position + 10, color);
  draw__H_small(row_position, col_position + 16, color);
  draw__O_small(row_position, col_position + 21, color);
  draw__N_small(row_position, col_position + 26, color);
  draw__Y_small(row_position, col_position + 31, color);
  if (color != Black) {
    vTaskDelay(wait_time);
  }

  // No space between words to fit whole name on one line
  draw__Z_small(row_position, col_position + 36, color);
  draw__U_small(row_position, col_position + 41, color);
  draw__N_small(row_position, col_position + 46, color);
  draw__I_small(row_position, col_position + 51, color);
  draw__N_small(row_position, col_position + 55, color);
  draw__O_small(row_position, col_position + 60, color);
  if (color != Black) {
    vTaskDelay(wait_time);
  }
}

static void draw_present_and_wait(uint8_t row_position, uint8_t col_position, uint16_t wait_time, color_code color) {
  draw__P_small(row_position, col_position + 0, color);
  draw__R_small(row_position, col_position + 5, color);
  draw__E_small(row_position, col_position + 10, color);
  draw__S_small(row_position, col_position + 15, color);
  draw__E_small(row_position, col_position + 20, color);
  draw__N_small(row_position, col_position + 25, color);
  draw__T_small(row_position, col_position + 30, color);
  if (color != Black) {
    vTaskDelay(wait_time);
  }
}

static void draw_DDR_at_top_of_screen_and_RTOS_at_bottom(color_code DDR_color, color_code RTOS_color) {
  draw__title_letter_D(0, 14, DDR_color);
  draw__title_letter_D(0, 20, DDR_color);
  draw__title_letter_R(0, 26, DDR_color);

  draw__title_letter_R(54, 26, RTOS_color);
  draw__title_letter_T(54, 32, RTOS_color);
  draw__title_letter_O(54, 38, RTOS_color);
  draw__title_letter_S(54, 44, RTOS_color);
}

static void move_DDR_down_and_RTOS_up_line_by_line(uint32_t delay_between_shifts__ms, color_code DDR_color,
                                                   color_code RTOS_color) {
  for (uint8_t row = 0; row <= 25; row++) {
    draw__title_letter_D(row, 14, Black);
    draw__title_letter_D(row + 1, 14, DDR_color);
    draw__title_letter_D(row, 20, Black);
    draw__title_letter_D(row + 1, 20, DDR_color);
    draw__title_letter_R(row, 26, Black);
    draw__title_letter_R(row + 1, 26, DDR_color);

    draw__title_letter_R(54 - row, 26, Black);
    draw__title_letter_R(54 - row - 1, 26, RTOS_color);
    draw__title_letter_T(54 - row, 32, Black);
    draw__title_letter_T(54 - row - 1, 32, RTOS_color);
    draw__title_letter_O(54 - row, 38, Black);
    draw__title_letter_O(54 - row - 1, 38, RTOS_color);
    draw__title_letter_S(54 - row, 44, Black);
    draw__title_letter_S(54 - row - 1, 44, RTOS_color);

    vTaskDelay(delay_between_shifts__ms);
  }
}

static void merge_DDR_and_RTOS_and_make_R_new_color(color_code R_color, color_code DDR_color, color_code RTOS_color) {
  draw__title_letter_D(26, 14, Black);
  draw__title_letter_D(26 + 1, 14, DDR_color);
  draw__title_letter_D(26, 20, Black);
  draw__title_letter_D(26 + 1, 20, DDR_color);
  draw__title_letter_R(26, 26, Black);
  draw__title_letter_R(54 - 26, 26, Black);
  draw__title_letter_R(27, 26, R_color);
  draw__title_letter_T(54 - 26, 32, Black);
  draw__title_letter_T(54 - 26 - 1, 32, RTOS_color);
  draw__title_letter_O(54 - 26, 38, Black);
  draw__title_letter_O(54 - 26 - 1, 38, RTOS_color);
  draw__title_letter_S(54 - 26, 44, Black);
  draw__title_letter_S(54 - 26 - 1, 44, RTOS_color);
}

static void wait_then_draw_press_any_to_start(uint32_t wait_time__ms) {
  vTaskDelay(wait_time__ms);

  uint8_t top_col_index = 12;
  uint8_t middle_col_index = 27;
  uint8_t bottom_col_index = 14;

  const uint8_t top_row_index = 39;
  const uint8_t middle_row_index = 46;
  const uint8_t bottom_row_index = 53;

  const uint8_t standard_letter_offset = 5;
  const uint8_t wide_letter_offset = 6;
  const uint8_t standard_letter_and_space_offset = 6;

  draw__P_small(top_row_index, top_col_index, White);
  draw__R_small(top_row_index, top_col_index += standard_letter_offset, White);
  draw__E_small(top_row_index, top_col_index += standard_letter_offset, White);
  draw__S_small(top_row_index, top_col_index += standard_letter_offset, White);
  draw__S_small(top_row_index, top_col_index += standard_letter_offset, White);

  draw__A_small(top_row_index, top_col_index += standard_letter_and_space_offset, White);
  draw__N_small(top_row_index, top_col_index += standard_letter_offset, White);
  draw__Y_small(top_row_index, top_col_index += standard_letter_offset, White);

  draw__T_small(middle_row_index, middle_col_index, White);
  draw__O_small(middle_row_index, middle_col_index += wide_letter_offset, White);

  draw__S_small(bottom_row_index, bottom_col_index += standard_letter_and_space_offset, White);
  draw__T_small(bottom_row_index, bottom_col_index += standard_letter_offset, White);
  draw__A_small(bottom_row_index, bottom_col_index += wide_letter_offset, White);
  draw__R_small(bottom_row_index, bottom_col_index += standard_letter_offset, White);
  draw__T_small(bottom_row_index, bottom_col_index += standard_letter_offset, White);
}

static void wait_for_button_input(void) {
  while (!press_to_start) {
    vTaskDelay(10);
  }
}

static void clear_board_and_wait(uint32_t wait_time__ms) {
  wipe_screen_right_to_left();
  vTaskDelay(wait_time__ms);
}

static void start_song_select_and_suspend_this_task(void) {
  vTaskResume(song_select_sequence);
  vTaskSuspend(title_logo_sequence);
}

static void wipe_screen_right_to_left(void) {
  for (uint8_t i = 63; i >= 1; i--) {
    led_driver__draw_col(i, Black);
    vTaskDelay(10);
  }
}
/***** HELPER FUNCTIONS END *****/