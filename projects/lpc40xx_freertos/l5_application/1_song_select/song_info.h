#pragma once

typedef enum {
  ghost_voices = 1,
  neverafter = 2,
  ghost_voices_raito = 3,
  neverafter_ss = 4,
  dont_let_me = 5,
  weightless_ss = 6,
  dont_let_me_ss = 7,
  weightless = 8,
  blurred = 9,
} song_name_e;