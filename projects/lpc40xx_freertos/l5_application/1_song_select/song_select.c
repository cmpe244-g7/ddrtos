#include "song_select.h"

#include "button.h"
#include "game_logic.h"
#include "gpio0_isr.h"
#include "l3_drivers/gpio.h"
#include "mp3_module.h"
#include "semphr.h"
#include "song_info.h"
#include "symbols.h"

#include <stdint.h>
#include <stdlib.h>

static volatile uint8_t song_select_position = 0;
static volatile bool song_chosen = false; // TODO: Change to semaphore
static SemaphoreHandle_t switch_pressed_song_preview;

/***** HELPER FUNCTIONS *****/
static void draw__choose_your_song(color_code color);
static void draw__song_name__weightless(void);
static void draw__song_name__dont_let_me_sleep(void);
static void draw__song_name__neverafter(void);
static void suspend_secondary_tasks(void);
static void delay_pause_clear_screen_then_delay_again(const uint32_t first_delay, const uint32_t second_delay);
static void start_game_logic_and_suspend_this_task(void);
static void wipe_screen_left_to_right(void);

void song_select(void *p) {
  switch_pressed_song_preview = xSemaphoreCreateBinary();
  while (1) {
    set__game_state(song_select__state);
    mp3_module__play(UART__3, ghost_voices_raito);
    vTaskResume(strobe_song_handler);
    vTaskResume(song_preview_handler);

    draw__song_name__weightless();
    draw__song_name__dont_let_me_sleep();
    draw__song_name__neverafter();

    song_chosen = false;
    song_select_position = 0;

    while (!song_chosen) {
      vTaskDelay(10);
    }

    set__game_state(song_select_wait__state);

    mp3_module__play(sound_effect__mp3, 5);

    suspend_secondary_tasks();
    led_driver__clear_board();

    const uint8_t arrow_row__position_1 = 23;
    const uint8_t arrow_row__position_2 = 37;
    const uint8_t arrow_row__position_3 = 51;

    const uint8_t arrow_col__position_all = 59;

    switch (song_select_position) {
    case 1:
      draw__song_name__weightless();
      draw__selector_arrow(arrow_row__position_1, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__weightless();
      draw__selector_arrow(arrow_row__position_1, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__weightless();
      draw__selector_arrow(arrow_row__position_1, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__weightless();
      draw__selector_arrow(arrow_row__position_1, arrow_col__position_all, Green);
      vTaskDelay(1000);
      break;

    case 2:
      draw__song_name__dont_let_me_sleep();
      draw__selector_arrow(arrow_row__position_2, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__dont_let_me_sleep();
      draw__selector_arrow(arrow_row__position_2, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__dont_let_me_sleep();
      draw__selector_arrow(arrow_row__position_2, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      draw__song_name__dont_let_me_sleep();
      draw__selector_arrow(arrow_row__position_2, arrow_col__position_all, Green);
      vTaskDelay(1000);
      break;

    case 3:
      draw__song_name__neverafter();
      draw__selector_arrow(arrow_row__position_3, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__neverafter();
      draw__selector_arrow(arrow_row__position_3, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__neverafter();
      draw__selector_arrow(arrow_row__position_3, arrow_col__position_all, Green);
      vTaskDelay(100);
      led_driver__clear_board();
      vTaskDelay(100);
      draw__song_name__neverafter();
      draw__selector_arrow(arrow_row__position_3, arrow_col__position_all, Green);
      vTaskDelay(1000);
      break;

    default:
      fprintf(stderr, "Err: keep song on screen default case\n");
    }

    vTaskDelay(2000);
    vTaskResume(song_pause_handler);
    wipe_screen_left_to_right();

    start_game_logic_and_suspend_this_task();
  }
}

void strobe_choose_your_song(void *p) {
  const uint16_t beat_length__ghost_voices_raito_remix = 469;
  TickType_t xLastWakeTime;
  while (1) {
    xLastWakeTime = xTaskGetTickCount();
    color_code random_color = (rand() % 7) + 1;
    draw__choose_your_song(random_color);
    led_driver__draw_col(0, Blue);
    led_driver__draw_col(63, Blue);
    led_driver__draw_row(0, Red);
    led_driver__draw_row(63, Red);
    vTaskDelayUntil(&xLastWakeTime, beat_length__ghost_voices_raito_remix);

    xLastWakeTime = xTaskGetTickCount();
    random_color = (rand() % 7) + 1;
    draw__choose_your_song(random_color);
    led_driver__draw_col(0, Red);
    led_driver__draw_col(63, Red);
    led_driver__draw_row(0, Blue);
    led_driver__draw_row(63, Blue);
    vTaskDelayUntil(&xLastWakeTime, beat_length__ghost_voices_raito_remix);
  }
}

void sleep_on_semaphore_song_preview(void *p) {
  const uint8_t arrow_row__position_1 = 23;
  const uint8_t arrow_row__position_2 = 37;
  const uint8_t arrow_row__position_3 = 51;

  const uint8_t arrow_col__position_all = 59;

  while (1) {
    if (xSemaphoreTake(switch_pressed_song_preview, portMAX_DELAY)) {
      switch (song_select_position) {
      case 1:
        draw__selector_arrow(arrow_row__position_2, arrow_col__position_all, Black);
        draw__selector_arrow(arrow_row__position_3, arrow_col__position_all, Black);
        draw__selector_arrow(arrow_row__position_1, arrow_col__position_all, Green);
        mp3_module__play(sound_effect__mp3, 4);
        mp3_module__play(song__mp3, weightless_ss);
        break;

      case 2:
        draw__selector_arrow(arrow_row__position_1, arrow_col__position_all, Black);
        draw__selector_arrow(arrow_row__position_3, arrow_col__position_all, Black);
        draw__selector_arrow(arrow_row__position_2, arrow_col__position_all, Green);
        mp3_module__play(sound_effect__mp3, 4);
        mp3_module__play(song__mp3, dont_let_me_ss);
        break;

      case 3:
        draw__selector_arrow(arrow_row__position_1, arrow_col__position_all, Black);
        draw__selector_arrow(arrow_row__position_2, arrow_col__position_all, Black);
        draw__selector_arrow(arrow_row__position_3, arrow_col__position_all, Green);
        mp3_module__play(sound_effect__mp3, 4);
        mp3_module__play(song__mp3, neverafter_ss);
        break;

      default:
        fprintf(stderr, "Err: song select default case\n");
      }
    }
  }
}

void pause_song_task_to_elim_starting_delay(void *p) {
  while (1) {
    mp3_module__reset_playback(song__mp3);
    vTaskSuspend(song_pause_handler);
  }
}

void song_menu_move_selector_up__handler(void) {
  switch (song_select_position) {
  case 0:
    break;

  case 1:
    break;

  case 2:
    song_select_position = 1;
    xSemaphoreGiveFromISR(switch_pressed_song_preview, NULL);
    break;

  case 3:
    song_select_position = 2;
    xSemaphoreGiveFromISR(switch_pressed_song_preview, NULL);
    break;

  default:
    fprintf(stderr, "Err: song selector up default case - %d\n", song_select_position);
  }
}

void song_menu_move_selector_down__handler(void) {
  switch (song_select_position) {
  case 0:
    song_select_position = 1;
    xSemaphoreGiveFromISR(switch_pressed_song_preview, NULL);
    break;

  case 1:
    song_select_position = 2;
    xSemaphoreGiveFromISR(switch_pressed_song_preview, NULL);
    break;

  case 2:
    song_select_position = 3;
    xSemaphoreGiveFromISR(switch_pressed_song_preview, NULL);
    break;

  case 3:
    break;

  default:
    fprintf(stderr, "Err: song selector down default case- %d\n", song_select_position);
  }
}

void song_menu_confirm_song_choice__handler(void) {
  switch (song_select_position) {
  case 0:
    break;

  case 1:
    song_chosen = true;
    break;

  case 2:
    song_chosen = true;
    break;

  case 3:
    song_chosen = true;
    break;

  default:
    fprintf(stderr, "Err: song selector right default case- %d\n", song_select_position);
  }
}

uint8_t get__selected_song(void) { return song_select_position; }

/***** HELPER FUNCTIONS BEGIN *****/
static void draw__choose_your_song(color_code color) {
  const uint8_t standard_letter_offset = 5;
  const uint8_t standard_letter_and_space_offset = 6;

  uint8_t row_index = 2;
  uint8_t col_index = 7;

  draw__C_small(row_index, col_index, color);
  draw__H_small(row_index, col_index += standard_letter_offset, color);
  draw__O_small(row_index, col_index += standard_letter_offset, color);
  draw__O_small(row_index, col_index += standard_letter_offset, color);
  draw__S_small(row_index, col_index += standard_letter_offset, color);
  draw__E_small(row_index, col_index += standard_letter_offset, color);

  draw__Y_small(row_index, col_index += standard_letter_and_space_offset, color);
  draw__O_small(row_index, col_index += standard_letter_offset, color);
  draw__U_small(row_index, col_index += standard_letter_offset, color);
  draw__R_small(row_index, col_index += standard_letter_offset, color);

  row_index += 6;
  col_index = 18;

  draw__S_small(row_index, col_index += standard_letter_offset, color);
  draw__O_small(row_index, col_index += standard_letter_offset, color);
  draw__N_small(row_index, col_index += standard_letter_offset, color);
  draw__G_small(row_index, col_index += standard_letter_offset, color);
}

static void draw__song_name__weightless(void) {
  const uint8_t small_letter_offset = 4;
  const uint8_t standard_letter_offset = 5;
  const uint8_t large_letter_offset = 6;
  const uint8_t standard_letter_and_space_offset = 6;

  uint8_t row_index = 20;
  uint8_t col_index = 2;

  draw__W_small(row_index, col_index, White);
  draw__E_small(row_index, col_index += large_letter_offset, White);
  draw__I_small(row_index, col_index += standard_letter_offset, White);
  draw__G_small(row_index, col_index += small_letter_offset, White);
  draw__H_small(row_index, col_index += standard_letter_offset, White);
  draw__T_small(row_index, col_index += standard_letter_offset, White);
  draw__L_small(row_index, col_index += large_letter_offset, White);
  draw__E_small(row_index, col_index += standard_letter_offset, White);
  draw__S_small(row_index, col_index += standard_letter_offset, White);
  draw__S_small(row_index, col_index += standard_letter_offset, White);

  row_index += 6;
  col_index = 2;

  draw__B_small(row_index, col_index, White);
  draw__E_small(row_index, col_index += standard_letter_offset, White);
  draw__N_small(row_index, col_index += standard_letter_offset, White);

  draw__B_small(row_index, col_index += standard_letter_and_space_offset, White);
  draw__O_small(row_index, col_index += standard_letter_offset, White);
  draw__H_small(row_index, col_index += standard_letter_offset, White);
  draw__M_small(row_index, col_index += standard_letter_offset, White);
  draw__E_small(row_index, col_index += large_letter_offset, White);
  draw__R_small(row_index, col_index += standard_letter_offset, White);
}

static void draw__song_name__dont_let_me_sleep(void) {
  const uint8_t tiny_letter_offset = 2;
  const uint8_t small_letter_offset = 4;
  const uint8_t standard_letter_offset = 5;
  const uint8_t large_letter_offset = 6;
  const uint8_t large_letter_and_space_offset = 7;

  uint8_t row_index = 34;
  uint8_t col_index = 2;

  draw__D_small(row_index, col_index, White);
  draw__O_small(row_index, col_index += standard_letter_offset, White);
  draw__N_small(row_index, col_index += standard_letter_offset, White);
  draw__apostrophe(row_index, col_index += standard_letter_offset, White);
  draw__T_small(row_index, col_index += tiny_letter_offset, White);

  draw__L_small(row_index, col_index += large_letter_and_space_offset, White);
  draw__E_small(row_index, col_index += standard_letter_offset, White);
  draw__T_small(row_index, col_index += standard_letter_offset, White);

  draw__M_small(row_index, col_index += large_letter_and_space_offset, White);
  draw__E_small(row_index, col_index += large_letter_offset, White);

  row_index += 6;
  col_index = 2;

  draw__lowercase_i_small(row_index, col_index, White);
  draw__underscore(row_index, col_index += tiny_letter_offset, White);
  draw__O_small(row_index, col_index += small_letter_offset, White);
}

static void draw__song_name__neverafter(void) {
  const uint8_t small_letter_offset = 4;
  const uint8_t standard_letter_offset = 5;
  const uint8_t large_letter_offset = 6;
  const uint8_t standard_letter_and_space_offset = 6;

  uint8_t row_index = 48;
  uint8_t col_index = 2;

  draw__N_small(row_index, col_index, White);
  draw__E_small(row_index, col_index += standard_letter_offset, White);
  draw__V_small(row_index, col_index += standard_letter_offset, White);
  draw__E_small(row_index, col_index += small_letter_offset, White);
  draw__R_small(row_index, col_index += standard_letter_offset, White);
  draw__A_small(row_index, col_index += standard_letter_offset, White);
  draw__F_small(row_index, col_index += standard_letter_offset, White);
  draw__T_small(row_index, col_index += standard_letter_offset, White);
  draw__E_small(row_index, col_index += large_letter_offset, White);
  draw__R_small(row_index, col_index += standard_letter_offset, White);

  row_index += 6;
  col_index = 2;

  draw__J_small(row_index, col_index, White);
  draw__period(row_index, col_index += standard_letter_offset, White);

  draw__H_small(row_index, col_index += standard_letter_and_space_offset, White);
  draw__A_small(row_index, col_index += standard_letter_offset, White);
  draw__W_small(row_index, col_index += standard_letter_offset, White);
  draw__K_small(row_index, col_index += large_letter_offset, White);
  draw__E_small(row_index, col_index += standard_letter_offset, White);
  draw__S_small(row_index, col_index += standard_letter_offset, White);
}

static void suspend_secondary_tasks(void) { vTaskSuspend(strobe_song_handler); }

static void delay_pause_clear_screen_then_delay_again(uint32_t first_delay, uint32_t second_delay) {
  vTaskDelay(first_delay);
  mp3_module__reset_playback(song__mp3);
  led_driver__clear_board();
  vTaskDelay(second_delay);
}

static void start_game_logic_and_suspend_this_task(void) {
  vTaskResume(game_logic_sequence);
  vTaskSuspend(song_select_sequence);
}

static void wipe_screen_left_to_right(void) {
  for (uint8_t i = 0; i < 64; i++) {
    led_driver__draw_col(i, Black);
    vTaskDelay(10);
  }
}
/***** HELPER FUNCTIONS END *****/
