#pragma once

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t song_select_sequence;
TaskHandle_t strobe_song_handler;
TaskHandle_t song_preview_handler;
TaskHandle_t song_pause_handler;

void song_select(void *p);
void strobe_choose_your_song(void *p);
void sleep_on_semaphore_song_preview(void *p);
void pause_song_task_to_elim_starting_delay(void *p);

void song_menu_move_selector_up__handler(void);
void song_menu_move_selector_down__handler(void);
void song_menu_confirm_song_choice__handler(void);

uint8_t get__selected_song(void);