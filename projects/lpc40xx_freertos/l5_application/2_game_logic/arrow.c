#include "arrow.h"

#include "game_logic.h"
#include "led_matrix.h"
#include "symbols.h"

#include <stdint.h>

static void clear_arrow_inside(uint8_t start_clear, uint8_t end_clear) {
  led_driver__draw_partial_row(8, start_clear, end_clear, Black);
  led_driver__draw_partial_row(9, start_clear, end_clear, Black);
  led_driver__draw_partial_row(10, start_clear, end_clear, Black);
  led_driver__draw_partial_row(11, start_clear, end_clear, Black);
  led_driver__draw_partial_row(12, start_clear, end_clear, Black);
  led_driver__draw_partial_row(13, start_clear, end_clear, Black);
  led_driver__draw_partial_row(14, start_clear, end_clear, Black);
  led_driver__draw_partial_row(15, start_clear, end_clear, Black);
  led_driver__draw_partial_row(16, start_clear, end_clear, Black);
}

static void clear_arrow_outside(uint8_t start_clear, uint8_t end_clear, uint8_t center) {
  const uint8_t start_row = 17;
  const uint8_t end_row = 22;

  for (uint8_t row = start_row; row <= end_row; ++row) {
    if (led_driver__check_if_pixel_is_filled(row, center)) {
      led_driver__draw_partial_row(row, start_clear, end_clear, Black);
    } else {
      break;
    }
  }
}

void clear_left_arrow_after_hit(void) {
  const uint8_t start_clear = 9;
  const uint8_t end_clear = 19;
  const uint8_t center = 13;

  clear_arrow_inside(start_clear, end_clear);
  clear_arrow_outside(start_clear, end_clear, center);

  draw__left_arrow_outline__static(Red);
}

void clear_up_arrow_after_hit(void) {
  const uint8_t start_clear = 21;
  const uint8_t end_clear = 30;
  const uint8_t center = 25;

  clear_arrow_inside(start_clear, end_clear);
  clear_arrow_outside(start_clear, end_clear, center);

  draw__up_arrow_outline__static(Red);
}

void clear_down_arrow_after_hit(void) {
  const uint8_t start_clear = 32;
  const uint8_t end_clear = 41;
  const uint8_t center = 36;

  clear_arrow_inside(start_clear, end_clear);
  clear_arrow_outside(start_clear, end_clear, center);

  draw__down_arrow_outline__static(Red);
}

void clear_right_arrow_after_hit(void) {
  const uint8_t start_clear = 43;
  const uint8_t end_clear = 53;
  const uint8_t center = 49;

  clear_arrow_inside(start_clear, end_clear);
  clear_arrow_outside(start_clear, end_clear, center);

  draw__right_arrow_outline__static(Red);
}
