#include "beatmap__dont_sleep.h"

#include "led_matrix.h"
#include "symbols.h"

#include <stdlib.h>

void beatmap__dont_sleep(void *p) {
  const uint16_t length_of_one_beat = 466;
  const uint16_t length_of_two_beats = length_of_one_beat * 2;
  TickType_t xLastWakeTime;

  while (1) {
    const uint16_t beat_length__first_half = length_of_one_beat / 2;
    const uint16_t beat_length__second_half = length_of_one_beat - beat_length__first_half;
    for (uint8_t i = 0; i < 32; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

      xLastWakeTime = xTaskGetTickCount();
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    vTaskDelay(100);
    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, length_of_two_beats);
    }

    for (uint8_t i = 0; i < 2; i++) {
      for (uint8_t j = 0; j < 4; j++) {
        xLastWakeTime = xTaskGetTickCount();
        draw__left_arrow_bottom__static(Yellow);
        vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
      }

      for (uint8_t j = 0; j < 4; j++) {
        xLastWakeTime = xTaskGetTickCount();
        draw__up_arrow_bottom__static(Yellow);
        vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
      }

      for (uint8_t j = 0; j < 4; j++) {
        xLastWakeTime = xTaskGetTickCount();
        draw__down_arrow_bottom__static(Yellow);
        vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
      }

      for (uint8_t j = 0; j < 8; j++) {
        xLastWakeTime = xTaskGetTickCount();
        draw__right_arrow_bottom__static(Yellow);
        vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
      }

      for (uint8_t j = 0; j < 4; j++) {
        xLastWakeTime = xTaskGetTickCount();
        draw__down_arrow_bottom__static(Yellow);
        vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
      }

      for (uint8_t j = 0; j < 4; j++) {
        xLastWakeTime = xTaskGetTickCount();
        draw__up_arrow_bottom__static(Yellow);
        vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
      }

      for (uint8_t j = 0; j < 4; j++) {
        xLastWakeTime = xTaskGetTickCount();
        draw__left_arrow_bottom__static(Yellow);
        vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
      }
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, length_of_two_beats);
    }

    for (uint8_t i = 0; i < 8; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);
    }

    xLastWakeTime = xTaskGetTickCount();
    // skip right so it doesn't  "double up"
    vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

    xLastWakeTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(Yellow);
    vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

    xLastWakeTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(Yellow);
    vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

    xLastWakeTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(Yellow);
    vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

    for (uint8_t i = 0; i < 7; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);
    }

    for (uint8_t i = 0; i < 8; i++) {
      xLastWakeTime = xTaskGetTickCount();
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

      xLastWakeTime = xTaskGetTickCount();
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__first_half);

      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, beat_length__second_half);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);

      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    // outro
    for (uint8_t i = 0; i < 26; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Yellow);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    vTaskSuspend(beatmap__dont_sleep__handler);
  }
}