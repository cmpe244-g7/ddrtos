#pragma once

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t beatmap__dont_sleep__handler;

void beatmap__dont_sleep(void *p);