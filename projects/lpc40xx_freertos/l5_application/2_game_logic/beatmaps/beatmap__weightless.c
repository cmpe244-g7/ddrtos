#include "beatmap__weightless.h"

#include "led_matrix.h"
#include "symbols.h"

#include <stdlib.h>

void beatmap__weightless(void *p) {
  const uint16_t length_of_one_beat = 470;
  const uint16_t length_of_two_beats = length_of_one_beat * 2;
  TickType_t xLastWakeTime;

  while (1) {

    for (uint8_t i = 0; i < 8; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 8; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_two_beats);
    }

    for (uint8_t i = 0; i < 8; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_two_beats);
    }

    for (uint8_t i = 0; i < 64; i++) {
      xLastWakeTime = xTaskGetTickCount();
      uint8_t arrow_choice = rand() % 4;
      switch (arrow_choice) {
      case 0:
        draw__left_arrow_bottom__static(Blue);
        break;

      case 1:
        draw__up_arrow_bottom__static(Blue);
        break;

      case 2:
        draw__down_arrow_bottom__static(Blue);
        break;

      case 3:
        draw__right_arrow_bottom__static(Blue);
        break;
      }
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    vTaskDelay(25);

    for (uint8_t i = 0; i < 128; i++) {
      xLastWakeTime = xTaskGetTickCount();
      uint8_t arrow_choice = rand() % 4;
      switch (arrow_choice) {
      case 0:
        draw__left_arrow_bottom__static(Blue);
        break;

      case 1:
        draw__up_arrow_bottom__static(Blue);
        break;

      case 2:
        draw__down_arrow_bottom__static(Blue);
        break;

      case 3:
        draw__right_arrow_bottom__static(Blue);
        break;
      }
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    vTaskDelay(50);

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    for (uint8_t i = 0; i < 16; i++) {
      xLastWakeTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(Blue);
      vTaskDelayUntil(&xLastWakeTime, length_of_one_beat);
    }

    vTaskSuspend(beatmap__weightless__handler);
  }
}