#pragma once

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t beatmap__weightless__handler;

void beatmap__weightless(void *p);