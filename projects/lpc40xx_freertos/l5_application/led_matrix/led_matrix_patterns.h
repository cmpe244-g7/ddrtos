#pragma once

void led_patterns__fill_rows_R_G_B_then_clear(void);
void led_patterns__set_random_pixels_then_clear(void);
void led_patterns__increasingly_fast_row_color_changes_then_clear(void);
void led_patterns__write_hi(void);
void led_patterns__flash_square_124_bpm(void);