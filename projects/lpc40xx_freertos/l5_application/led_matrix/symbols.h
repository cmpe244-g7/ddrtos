#pragma once

#include "led_matrix.h"
#include <stdint.h>

/*

---*-----
--**-----
-********
*********
*********
-********
--**-----
---*-----

*/

/***** ARROWS *****/
void draw__left_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__up_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__down_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__right_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);

/***** STATICS *****/
// Note: Static symbols denote those that will only ever be placed at a single location
void draw__left_arrow_outline__static(color_code color);
void draw__up_arrow_outline__static(color_code color);
void draw__down_arrow_outline__static(color_code color);
void draw__right_arrow_outline__static(color_code color);

void erase__left_arrow_outline__static(void);
void erase__up_arrow_outline__static(void);
void erase__down_arrow_outline__static(void);
void erase__right_arrow_outline__static(void);

void draw__left_arrow_bottom__static(color_code color);
void draw__up_arrow_bottom__static(color_code color);
void draw__down_arrow_bottom__static(color_code color);
void draw__right_arrow_bottom__static(color_code color);

/***** SPECIAL SYMBOLS / ENVIRONMENT *****/
void draw__selector_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__lane(color_code);

/***** BIG (title) LETTERS *****/
void draw__title_letter_D(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__title_letter_R(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__title_letter_T(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__title_letter_O(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__title_letter_S(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);

/**** SMALL (general text) LETTERS *****/
// All small letters are 4 rows x 5 columns (standard offset) unless specifically noted
void draw__A_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__B_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__C_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__D_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__E_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__F_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__G_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__H_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__I_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col,
                   color_code color); // 3 x 5
void draw__J_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__K_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__L_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__M_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__N_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__O_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__P_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__Q_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__R_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__S_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__T_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col,
                   color_code color); // 5 x 5
void draw__U_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__V_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col,
                   color_code color); // 3 x 5
void draw__W_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col,
                   color_code color); // 5 x 5
void draw__X_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__Y_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__Z_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);

/**** SMALL NUMBERS *****/
void draw__0_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__1_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col,
                   color_code color); // 3 x 5
void draw__2_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__3_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__4_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__5_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__6_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__7_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__8_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);
void draw__9_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);

/***** SPECIAL LETTERS *****/
void draw__lowercase_i_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);

/***** PUNCTUATION CHARACTERS *****/
void draw__apostrophe(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color); // 1 x 5
void draw__period(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color);     // 1 x 5
void draw__underscore(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color); // 3 x 5