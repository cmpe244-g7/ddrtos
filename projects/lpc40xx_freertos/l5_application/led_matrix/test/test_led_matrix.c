#include "unity.h"

#include <stdint.h>

#include "Mockgpio.h"
#include "Mocktask.h"

#include "led_matrix.c"

static gpio_s R1;
static gpio_s G1;
static gpio_s B1;
static gpio_s R2;
static gpio_s G2;
static gpio_s B2;
static gpio_s E;
static gpio_s A;
static gpio_s B;
static gpio_s C;
static gpio_s D;
static gpio_s CLK;
static gpio_s LAT;
static gpio_s OE;

void setUp(void) {
  R1 = (gpio_s){0};
  G1 = (gpio_s){0};
  B1 = (gpio_s){0};
  R2 = (gpio_s){0};
  G2 = (gpio_s){0};
  B2 = (gpio_s){0};
  E = (gpio_s){0};
  A = (gpio_s){0};
  B = (gpio_s){0};
  C = (gpio_s){0};
  D = (gpio_s){0};
  CLK = (gpio_s){0};
  LAT = (gpio_s){0};
  OE = (gpio_s){0};
}

void tearDown(void) {}

void test__clock_data() {
  gpio__set_Expect(CLK);
  gpio__reset_Expect(CLK);
  clock_data();
}

void test__enable_latch(void) {
  gpio__set_Expect(LAT);
  enable_latch();
}

void test__disable_latch(void) {
  gpio__reset_Expect(LAT);
  disable_latch();
}

void test__enable_output(void) {
  gpio__reset_Expect(OE);
  enable_output();
}

void test__disable_output(void) {
  gpio__set_Expect(OE);
  disable_output();
}

void test__clear_previously_selected_address(void) {
  gpio__reset_Expect(A);
  gpio__reset_Expect(B);
  gpio__reset_Expect(C);
  gpio__reset_Expect(D);
  gpio__reset_Expect(E);
  clear_previously_selected_address();
}

void test__set_all_matrix_data_pins_to_gpio_function_0(void) {
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 6, GPIO__FUNCTION_0_IO_PIN, R1);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 7, GPIO__FUNCTION_0_IO_PIN, G1);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 8, GPIO__FUNCTION_0_IO_PIN, B1);

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 26, GPIO__FUNCTION_0_IO_PIN, R2);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 25, GPIO__FUNCTION_0_IO_PIN, G2);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 31, GPIO__FUNCTION_0_IO_PIN, B2);

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 30, GPIO__FUNCTION_0_IO_PIN, E);

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 20, GPIO__FUNCTION_0_IO_PIN, A);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 23, GPIO__FUNCTION_0_IO_PIN, B);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 28, GPIO__FUNCTION_0_IO_PIN, C);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 29, GPIO__FUNCTION_0_IO_PIN, D);

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 0, GPIO__FUNCTION_0_IO_PIN, CLK);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 1, GPIO__FUNCTION_0_IO_PIN, LAT);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 2, GPIO__FUNCTION_0_IO_PIN, OE);
  set_all_matrix_data_pins_to_gpio_function_0();
}

void test__set_all_matrix_pins_as_output(void) {
  gpio__construct_as_output_ExpectAndReturn(R1.port_number, R1.pin_number, R1);
  gpio__construct_as_output_ExpectAndReturn(G1.port_number, G1.pin_number, G1);
  gpio__construct_as_output_ExpectAndReturn(B1.port_number, B1.pin_number, B1);
  gpio__construct_as_output_ExpectAndReturn(R2.port_number, R2.pin_number, R2);
  gpio__construct_as_output_ExpectAndReturn(G2.port_number, G2.pin_number, G2);
  gpio__construct_as_output_ExpectAndReturn(B2.port_number, B2.pin_number, B2);
  gpio__construct_as_output_ExpectAndReturn(E.port_number, E.pin_number, E);
  gpio__construct_as_output_ExpectAndReturn(A.port_number, A.pin_number, A);
  gpio__construct_as_output_ExpectAndReturn(B.port_number, B.pin_number, B);
  gpio__construct_as_output_ExpectAndReturn(C.port_number, C.pin_number, C);
  gpio__construct_as_output_ExpectAndReturn(D.port_number, D.pin_number, D);
  gpio__construct_as_output_ExpectAndReturn(CLK.port_number, CLK.pin_number, CLK);
  gpio__construct_as_output_ExpectAndReturn(LAT.port_number, LAT.pin_number, LAT);
  gpio__construct_as_output_ExpectAndReturn(OE.port_number, OE.pin_number, OE);
  set_all_matrix_pins_as_output();
}

void test__set_all_matrix_pins_to_low(void) {
  gpio__reset_Expect(R1);
  gpio__reset_Expect(G1);
  gpio__reset_Expect(B1);
  gpio__reset_Expect(R2);
  gpio__reset_Expect(G2);
  gpio__reset_Expect(B2);
  gpio__reset_Expect(E);
  gpio__reset_Expect(A);
  gpio__reset_Expect(B);
  gpio__reset_Expect(C);
  gpio__reset_Expect(D);
  gpio__reset_Expect(CLK);
  gpio__reset_Expect(LAT);
  gpio__reset_Expect(OE);
  set_all_matrix_pins_to_low();
}

void test__set_new_address_based_on_current_row__all_cases(void) {
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(0));
  gpio__set_Expect(A);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(1));
  gpio__set_Expect(B);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(2));
  gpio__set_Expect(A);
  gpio__set_Expect(B);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(3));
  gpio__set_Expect(C);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(4));
  gpio__set_Expect(A);
  gpio__set_Expect(B);
  gpio__set_Expect(C);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(7));
  gpio__set_Expect(D);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(8));
  gpio__set_Expect(A);
  gpio__set_Expect(B);
  gpio__set_Expect(C);
  gpio__set_Expect(D);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(15));
  gpio__set_Expect(E);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(16));
  gpio__set_Expect(A);
  gpio__set_Expect(B);
  gpio__set_Expect(C);
  gpio__set_Expect(D);
  gpio__set_Expect(E);
  TEST_ASSERT_TRUE(set_new_address_based_on_current_row(31));
  TEST_ASSERT_FALSE(set_new_address_based_on_current_row(32));
}

void test__choose_color_for_rows_0_through_31__all_cases(void) {
  TEST_ASSERT_FALSE(choose_color_for_rows_0_through_31(-1));
  gpio__reset_Expect(R1);
  gpio__reset_Expect(G1);
  gpio__reset_Expect(B1);
  TEST_ASSERT_TRUE(choose_color_for_rows_0_through_31(0)); // Black
  gpio__reset_Expect(R1);
  gpio__reset_Expect(G1);
  gpio__set_Expect(B1);
  TEST_ASSERT_TRUE(choose_color_for_rows_0_through_31(1)); // Blue
  gpio__set_Expect(R1);
  gpio__reset_Expect(G1);
  gpio__reset_Expect(B1);
  TEST_ASSERT_TRUE(choose_color_for_rows_0_through_31(4)); // Red
  gpio__set_Expect(R1);
  gpio__set_Expect(G1);
  gpio__set_Expect(B1);
  TEST_ASSERT_TRUE(choose_color_for_rows_0_through_31(7));  // White
  TEST_ASSERT_FALSE(choose_color_for_rows_0_through_31(8)); // Undefined
}

void test__choose_color_for_rows_32_through_61__all_cases(void) {
  TEST_ASSERT_FALSE(choose_color_for_rows_32_through_61(-1));
  gpio__reset_Expect(R2);
  gpio__reset_Expect(G2);
  gpio__reset_Expect(B2);
  TEST_ASSERT_TRUE(choose_color_for_rows_32_through_61(0)); // Black
  gpio__reset_Expect(R2);
  gpio__reset_Expect(G2);
  gpio__set_Expect(B2);
  TEST_ASSERT_TRUE(choose_color_for_rows_32_through_61(1)); // Blue
  gpio__set_Expect(R2);
  gpio__reset_Expect(G2);
  gpio__reset_Expect(B2);
  TEST_ASSERT_TRUE(choose_color_for_rows_32_through_61(4)); // Red
  gpio__set_Expect(R2);
  gpio__set_Expect(G2);
  gpio__set_Expect(B2);
  TEST_ASSERT_TRUE(choose_color_for_rows_32_through_61(7));  // White
  TEST_ASSERT_FALSE(choose_color_for_rows_32_through_61(8)); // Undefined
}
