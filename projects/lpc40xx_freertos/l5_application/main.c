#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "beatmap__dont_sleep.h"
#include "beatmap__neverafter.h"
#include "beatmap__weightless.h"
#include "button.h"
#include "game_logic.h"
#include "led_matrix.h"
#include "led_matrix_patterns.h"
#include "sj2_cli.h"
#include "song_select.h"
#include "symbols.h"
#include "title_sequence.h"

#include "mp3_module.h"

static void run_matrix(void *p);

/***** HELPER FUNCTIONS *****/
static void init_both_mp3_modules(void);
static void create_tasks_for_all_states(void);
static void create_tasks_for_title_screen(void);
static void create_tasks_for_song_select_screen(void);
static void create_tasks_for_gameplay_screen(void);
static void suspend_tasks_for_title_screen(void);
static void suspend_tasks_for_song_select_screen(void);
static void suspend_tasks_for_gameplay_screen(void);

int main(void) {
  sj2_cli__init();
  init_both_mp3_modules();
  all_buttons__init();
  led_driver__matrix_init();
  led_driver__clear_board();

  create_tasks_for_all_states();
  create_tasks_for_title_screen();
  create_tasks_for_song_select_screen();
  create_tasks_for_gameplay_screen();

  suspend_tasks_for_title_screen();
  suspend_tasks_for_song_select_screen();
  suspend_tasks_for_gameplay_screen();

  vTaskStartScheduler();
  return 0;
}

static void run_matrix(void *p) {
  while (1) {
    led_driver__update_display_from_matrix();
    // vTaskDelay(1); // vTaskDelay here will dim all rows except for the 31st and 63rd rows (0 indexed)
  }
}

/***** HELPER FUNCTIONS BEGIN *****/
static void init_both_mp3_modules(void) {
  mp3_module__init(sound_effect__mp3);
  mp3_module__init(song__mp3);
}

static void create_tasks_for_all_states(void) {
  xTaskCreate(run_matrix, "Refresh Matrix", 4096, NULL, PRIORITY_LOW, NULL);
}

static void create_tasks_for_title_screen(void) {
  xTaskCreate(title_logo, "Title", 1024, NULL, PRIORITY_LOW, &title_logo_sequence);
}

static void create_tasks_for_song_select_screen(void) {
  xTaskCreate(song_select, "Song_Menu", 1024, NULL, PRIORITY_LOW, &song_select_sequence);
  xTaskCreate(strobe_choose_your_song, "Strobe Effect Top Words", 1024, NULL, PRIORITY_LOW, &strobe_song_handler);
  xTaskCreate(sleep_on_semaphore_song_preview, "Song Preview Player", 1024, NULL, PRIORITY_LOW, &song_preview_handler);
  xTaskCreate(pause_song_task_to_elim_starting_delay, "Song pause", 1024, NULL, PRIORITY_LOW, &song_pause_handler);
}

static void create_tasks_for_gameplay_screen(void) {
  xTaskCreate(game_logic, "High Level Game Coordinator", 1024, NULL, PRIORITY_LOW, &game_logic_sequence);
  xTaskCreate(shift_matrix, "Shift Matrix", 1024, NULL, PRIORITY_LOW, &shift_matrix_handler);
  xTaskCreate(beatmap__dont_sleep, "Beat Map Dont Sleep", 1024, NULL, PRIORITY_HIGH, &beatmap__dont_sleep__handler);
  xTaskCreate(beatmap__neverafter, "Beat Map Neverafter", 1024, NULL, PRIORITY_HIGH, &beatmap__neverafter__handler);
  xTaskCreate(beatmap__weightless, "Draw Arrows", 1024, NULL, PRIORITY_HIGH, &beatmap__weightless__handler);
  xTaskCreate(update_score, "Draw Score", 1024, NULL, PRIORITY_LOW, &draw_score_handler);
  xTaskCreate(sleep_on_semaphore_sound_effect, "Signal Sound Effect", 1024, NULL, PRIORITY_LOW, &sound_effect_handler);
}

static void suspend_tasks_for_title_screen(void) {
  // for scons
  // vTaskSuspend(title_logo_sequence);
}

static void suspend_tasks_for_song_select_screen(void) {
  vTaskSuspend(song_select_sequence);
  vTaskSuspend(strobe_song_handler);
  vTaskSuspend(song_preview_handler);
  vTaskSuspend(song_pause_handler);
}

static void suspend_tasks_for_gameplay_screen(void) {
  vTaskSuspend(game_logic_sequence);
  vTaskSuspend(shift_matrix_handler);
  vTaskSuspend(beatmap__dont_sleep__handler);
  vTaskSuspend(beatmap__neverafter__handler);
  vTaskSuspend(beatmap__weightless__handler);
  vTaskSuspend(draw_score_handler);
  vTaskSuspend(sound_effect_handler);
}
/***** HELPER FUNCTIONS END *****/